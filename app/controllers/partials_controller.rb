class PartialsController < ApplicationController
  before_action :signed_in_user

  def new
    @partial = Partial.new
  end

  def create
    @debt = Debt.find(params[:debt_id])
    @partial = Partial.new(partial_params)

    if @partial.save
      @order = @debt.order_id
      @debt.payment(@partial, @debt)
      @debt.status(@debt, @order)
      @debt.save
      @order.save
      
      redirect_to @debt
      flash[:success] = "Recebimento de #{@partial.price} efetuado com Sucesso!"
    else
      redirect_to @debt
      flash[:error] = "Recebimento de #{@partial.price} nao pode ser efetuado"
    end
  end

  def destroy
    @partial = Partial.find(params[:id])
    @partial.destroy
    redirect_to debts_url
    flash[:success] = "Recebimento removido com Sucesso!"
  end
 
  private
    def partial_params
      params.require(:partial).permit(:debt_id, :price)
    end
end