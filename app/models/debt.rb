class Debt < ActiveRecord::Base
  has_many :partials, dependent: :destroy
  belongs_to :order
  validate :expiration_date_future, allow_blank: true
  accepts_nested_attributes_for :partials, allow_destroy: true

  def expiration_date_future
    if expiration_date.present? && expiration_date < Date.today
      errors.add(:expiration_date, "Data deve ser futura")
    end
  end

  def payment(partial, debt)
    debt.amount_paid = debt.amount_paid + partial.price
    debt.date_payment = DateTime.now
  end

  def status(debt, order)
    if debt.amount_paid >= debt.total
      order.status = "Paga"
    else
      order.status = "Recebimento Parcial"
    end
  end
end
