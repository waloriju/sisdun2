module CurrentCart
  extend ActiveSupport::Concern
  
  private
    def set_cart
      @cart = Cart.find(session[:cart_id])
    rescue ActiveRecord::RecordNotFound
      @cart = Cart.create
      session[:cart_id] = @cart.id
    end

    def debt_params
      params.require(:debt).permit(:order_id, :data_vencimento, :data_pagamento, :valor_toral, :valor_pago, :valor_restante).merge(:valor_toral, :valor_restante => params[:price], :order_id => params[:order_id])
    end
end