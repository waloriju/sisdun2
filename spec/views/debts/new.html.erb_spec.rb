require 'spec_helper'

describe "debts/new" do
  before(:each) do
    assign(:debt, stub_model(Debt,
      :order => nil,
      :valor_toral => "9.99",
      :valor_pago => "9.99",
      :valor_restante => "9.99"
    ).as_new_record)
  end

  it "renders new debt form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", debts_path, "post" do
      assert_select "input#debt_order[name=?]", "debt[order]"
      assert_select "input#debt_valor_toral[name=?]", "debt[valor_toral]"
      assert_select "input#debt_valor_pago[name=?]", "debt[valor_pago]"
      assert_select "input#debt_valor_restante[name=?]", "debt[valor_restante]"
    end
  end
end
