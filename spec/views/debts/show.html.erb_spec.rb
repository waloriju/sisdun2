require 'spec_helper'

describe "debts/show" do
  before(:each) do
    @debt = assign(:debt, stub_model(Debt,
      :order => nil,
      :valor_toral => "9.99",
      :valor_pago => "9.99",
      :valor_restante => "9.99"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/9.99/)
    rendered.should match(/9.99/)
    rendered.should match(/9.99/)
  end
end
