require 'spec_helper'

describe "debts/index" do
  before(:each) do
    assign(:debts, [
      stub_model(Debt,
        :order => nil,
        :valor_toral => "9.99",
        :valor_pago => "9.99",
        :valor_restante => "9.99"
      ),
      stub_model(Debt,
        :order => nil,
        :valor_toral => "9.99",
        :valor_pago => "9.99",
        :valor_restante => "9.99"
      )
    ])
  end

  it "renders a list of debts" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
  end
end
